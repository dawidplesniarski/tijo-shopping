package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GetProductsListTest {
    @Test
    void getAllProductsNames(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Onion",4,3);
        shoppingCart.addProducts("Carrot",1,5);
        shoppingCart.addProducts("Potatos",2,10);
        shoppingCart.addProducts("Hand Cream",10,1);
        shoppingCart.addProducts("Potatos",10,2);

        List<String> items = new ArrayList<>();
        items.add("Potatos");
        items.add("Carrot");
        items.add("Onion");
        items.add("Hand Cream");

        assertEquals(items, shoppingCart.getProductsNames());
    }
}
