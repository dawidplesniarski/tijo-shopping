package pl.edu.pwsztar;

public class Main {

    public static void main( String[] argv ) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Onion",4,100);
        shoppingCart.addProducts("Carrot",1,100);
        shoppingCart.addProducts("Potatos",2,100);
        shoppingCart.addProducts("Hand Cream",10,100);
        shoppingCart.addProducts("Potatos",2,109);

        System.out.println(shoppingCart.getAllProductsQuantity());
    }
}
